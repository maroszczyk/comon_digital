<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Books</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="css/app.css" type="text/css" rel="stylesheet">

</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="container">

        <div class="row">
            <div class="col-lg-1"><h1>Books:</h1></div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label for="availability">Availability</label>
                <select id="availability">
                    <option selected value="available_only">Only Available</option>
                    <option value="high"> &gt;10</option>
                    <option value="none">Not available</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="books" class="table table-bordered table-hover" data-uri="{{ route('book.index') }}" data-availability="#availability">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="empty-row">
                        <td colspan="3" class="text-center">Empty</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
