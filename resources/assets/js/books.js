var loadBooks = function(book_table, changed) {
    var table = $(book_table);
    var api_url = table.data('uri');
    var availability_selector = table.data('availability');
    var availability = changed === undefined ? $(availability_selector).val() : changed;

    axios.get(api_url, {
        params: {
            availability: availability
        }
    })
        .then(function (response) {
            table.find('tbody tr.data-row').remove();
            if (response.data.length == 0) {
                table.find('tbody tr.empty-row').show();
            } else {
                table.find('tbody tr.empty-row').hide();
            }

            var i = 1;
            for (var x in response.data) {
                var row = '<tr class="data-row"><td>' + (i++) +  '</td><td>' + response.data[x].title +'</td><td>' + response.data[x].amount + '</td></tr>'
                table.find('tbody').append(row);
            }
        })
        .catch(function (error) {
            alert(error);
        });

};

$(document).ready(function(){
    var book_table = '#books';
    loadBooks(book_table);

    $('#availability').on('change',function() {
        loadBooks(book_table, $(this).val());
    });
});