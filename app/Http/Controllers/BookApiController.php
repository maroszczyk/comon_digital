<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Book::where('amount', '>', 0);

        if ($request->has('availability')) {
            switch($request->get('availability')) {
                case 'none':
                    $books = Book::where('amount', '==', 0);
                break;
                case 'high':
                    $books = Book::where('amount', '>=', 10);
                break;
                case 'available_only':
                default:
                    $books = Book::where('amount', '>', 0);
                break;

            }
        }

        return $books->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('title') && $request->has('amount')) {
            $book = new Book;
            $book->title = $request->get('title');
            $book->amount = $request->get('amount', 0);
            $book->save();

            return response(array('status' => 'OK', 'id' => $book->id));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return $book;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $book->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        return ['success' => $book->delete() ? 'OK' : 'Error'];
    }
}
